package org.vanme;

import java.util.Collection;

public record Card(CardColor color, CardFace face) {

    public int computePoints(Collection<Card> cards) {
        return 0;
    }

}
