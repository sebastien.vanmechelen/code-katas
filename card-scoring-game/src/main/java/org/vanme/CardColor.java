package org.vanme;

public enum CardColor {

    HEARTS,
    DIAMONDS,
    CLUBS,
    SPADES

}
