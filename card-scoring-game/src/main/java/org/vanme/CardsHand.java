package org.vanme;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class CardsHand {

    private final List<Card> cards;

    public CardsHand() {
        this(Collections.emptyList());
    }

    public CardsHand(List<Card> cards) {
        this.cards = cards;
    }

    public CardsHand withCard(Card card) {
        return withCards(Collections.singleton(card));
    }

    public CardsHand withCards(Collection<Card> newCards) {
        return new CardsHand(Stream.concat(
                cards.stream(),
                newCards.stream()
        ).toList());
    }

    public int getScore() {
        return 0; // TODO
    }

}
