package org.vanme;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CardsHandTest {

    @Test
    void emptyHandShouldScore0() {
        assertThat(new CardsHand().getScore()).isZero();
    }

    @Test
    void oneCardHandThatShouldScoreItsCardPoints() {
        var hand = new CardsHand()
                .withCard(mockCardWithPointsOf(10));

        assertThat(hand.getScore()).isEqualTo(10);
    }

    @Test
    void twoCardsHandThatShouldScoreCardsSummedPoints() {
        var hand = new CardsHand().withCards(List.of(
                mockCardWithPointsOf(3),
                mockCardWithPointsOf(4)
        ));
        assertThat(hand.getScore()).isEqualTo(7);
    }

    private Card mockCardWithPointsOf(int points) {
        var card = mock(Card.class);
        when(card.computePoints(anyList()))
                .thenReturn(points);
        return card;
    }
}