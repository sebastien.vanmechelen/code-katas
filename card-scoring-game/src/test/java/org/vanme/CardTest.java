package org.vanme;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class CardTest {

    @ParameterizedTest
    @CsvSource(value = {
            "ACE,1",
            "TWO,2",
            "THREE,3",
            "FOUR,4",
            "FIVE,5",
            "SIX,6",
            "SEVEN,7",
            "EIGHT,8",
            "NINE,9",
            "TEN,10",
            "JACK,0", // Worth 0 here because no queen or king in hand
            "QUEEN,12",
            "KING,13"
    })
    void oneCardHandThatShouldScoreItsDefaultValue(CardFace face, int expectedScore) {
        var card = new Card(CardColor.HEARTS, face);
        assertThat(card.computePoints(Collections.emptyList()))
                .isEqualTo(expectedScore);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "QUEEN, HEARTS, 11",
            "QUEEN, SPADES, 0",
            "QUEEN, DIAMONDS, 0",
            "QUEEN, SPADES, 0",
            "KING, HEARTS, 11",
            "KING, SPADES, 0",
            "KING, DIAMONDS, 0",
            "KING, SPADES, 0",
    })
    void jackWorth11OnlyIfSameColorQueenOrKingInHand(CardFace cardFace, CardColor color, int expectedScore) {
        var jackHearts = new Card(CardColor.HEARTS, CardFace.JACK);
        var otherCards = List.of(new Card(color, cardFace));

        assertThat(jackHearts.computePoints(otherCards))
                .isEqualTo(expectedScore);
    }

    @Test
    void kingWorthTwoMoreForEachQueenInHand() {
        var kingHearts = new Card(CardColor.HEARTS, CardFace.KING);
        var queenSpades = new Card(CardColor.SPADES, CardFace.QUEEN);
        assertThat(kingHearts.computePoints(List.of(queenSpades)))
                .isEqualTo(15);

        assertThat(kingHearts.computePoints(List.of(queenSpades, queenSpades)))
                .isEqualTo(17);

        var nonQueenCard = new Card(CardColor.SPADES, CardFace.JACK);
        assertThat(kingHearts.computePoints(List.of(queenSpades, queenSpades, nonQueenCard)))
                .isEqualTo(17);
    }

    @Test
    void queenWorthOneLessForEachOtherQueenInHand() {
        var queenHearts = new Card(CardColor.HEARTS, CardFace.QUEEN);
        var anotherQueen = new Card(CardColor.SPADES, CardFace.QUEEN);
        assertThat(queenHearts.computePoints(List.of(anotherQueen)))
                .isEqualTo(11);

        assertThat(queenHearts.computePoints(List.of(anotherQueen, anotherQueen)))
                .isEqualTo(10);

        var nonQueenCard = new Card(CardColor.SPADES, CardFace.JACK);
        assertThat(queenHearts.computePoints(List.of(anotherQueen, anotherQueen, nonQueenCard)))
                .isEqualTo(10);
    }

    @Test
    void nonFigureWorthOneMoreIfCompleteCourtOfSameColor() {
        var completeHeartsCourt = Stream.of(CardFace.JACK, CardFace.QUEEN, CardFace.KING)
                .map(face -> new Card(CardColor.HEARTS, face))
                .toList();

        var tenHearts = new Card(CardColor.HEARTS, CardFace.TEN);
        assertThat(tenHearts.computePoints(completeHeartsCourt))
                .isEqualTo(11);

        var tenNonHearts = new Card(CardColor.SPADES, CardFace.TEN);
        assertThat(tenNonHearts.computePoints(completeHeartsCourt))
                .isEqualTo(11);
    }

    @Test
    void figureDoesNotWorthOneMoreIfCompleteCourtOfSameColor() {
        var jackAndKingOfHearts = Stream.of(CardFace.JACK, CardFace.KING)
                .map(face -> new Card(CardColor.HEARTS, face))
                .toList();

        var queenHearts = new Card(CardColor.HEARTS, CardFace.QUEEN);
        assertThat(queenHearts.computePoints(jackAndKingOfHearts))
                .isEqualTo(12);
    }
}