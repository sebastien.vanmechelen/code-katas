# Enum & functions Code Kata

## Pre-requisites
- Knowledge of Java programming
    - Java 17 is used here
- Confident in unit testing using for example:
    - [JUnit 5](https://junit.org/junit5/docs/current/user-guide/)
    - [AssetJ](http://joel-costigliola.github.io/assertj/)
    - ...

## Context
A card game where some cards are collected by players and the winner is the one with the highest score.
Each cards can score according to the other cards present in the player's hand.

### Scoring Rules
- 1-10 are worth their face value
- Jack, Queen, King are worth 11, 12, 13 respectively
- King has a +2 value for each Queen present in the hand (because the king likes ladies)
- Queen has a -1 value for each other queen present in the hand (because the queen is jealous)
- Jack worth 0 if there is no Queen or King of the same color in the hand (because no one to serve)
- If a court is complete (Jack, Queen, King of the same color), each non-figure card of this color is worth 1 more

## Exercise

- Implements the required scoring rules
- Make existing unit tests pass (and write missing ones if needed)