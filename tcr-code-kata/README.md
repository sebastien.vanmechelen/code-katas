# TCR Code Kata

## Pre-requisites
- Knowledge of Java programming
  - Java 17 is used here
- Confident in unit testing using for example:
  - [JUnit 5](https://junit.org/junit5/docs/current/user-guide/) 
  - [AssetJ](http://joel-costigliola.github.io/assertj/)
  - ...
- Basic knowledge of [Test Driven Development](https://testdriven.io/test-driven-development/).

## What means TCR?
TCR is the abbreviation for **(Test && Commit) || Rollback**.

I am advising you this [small article from Kent Beck](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864) before you start

## IntelliJ Plugin
### Limited WIP
Available [here](https://plugins.jetbrains.com/plugin/7655-limited-wip).

#### Settings
- Open IntelliJ Settings, in Other Settings > Limited WIP, under TCR section:
  - Tick "Enabled"
  - On passed test, select "commit" or "amend commit".
  - Tick "Notify on revert"
  - Tick "Don't revert tests"

## Exercise
#### Rules of the game

- As known from TDD:
    - Write tests before coding
    - After coding something, run all tests to ensure non-regression
- Do not commit code by yourself, instead, run your tests.

A basic `Person` class already exists with its unit tests.
From this base, we want to implement:

#### Step 0 - Ensure everything is correctly configured
* Run the test class `PersonTest`, tests should be green.
* Change the code of the `Person` class to make unit tests fail (for example by removing the `.toUpperCase()`).
* Run the test class `PersonTest` again, tests should be red and ensure that the plugin correctly reverted the modified code.

#### Step 1 - Body Mass Index computation
A person has a weight and a size, based on that, we can compute their [BMI](https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/english_bmi_calculator/bmi_calculator.html).

Let's assume we are working with the metric system, we can compute the BMI using the following [formula](https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/index.html#Interpreted):

`BMI = weight (kg) / (height (m) ^ 2)`

Example:
`72 / (1.78 ^ 2) = 22.72`

#### Step 2 - Interpretation of BMI
Based on our computed BMI, we want to know if the person is categorized as:
- Underweight `(BMI < 18.5)`
- Healthy Weight `(BMI >= 18.5 && < 25)`
- Overweight `(BMI >= 25 && < 30)`
- Obesity `(BMI > 30)`

#### Step 3 - Reflexion
How will your solution be impacted if we want to interpret the [BMI differently for an adult and a child/teen](https://www.cdc.gov/healthyweight/assessing/bmi/childrens_bmi/about_childrens_bmi.html#calculated)?

What did you learn from the exercise? What is your takeaway from this exercise?

## Resources

* [How Practicing TCR (Test && Commit || Revert) Reduces Batch Size](https://www.infoq.com/articles/test-commit-revert/)
* [TCR test && commit || revert -- Rope in Python by Kent Beck](https://www.youtube.com/playlist?list=PLlmVY7qtgT_nhLyIbeAaUlFOWbWT5y53t)

## Thanks
Thanks to Lucile Jeannot for beta-testing the exercise :-) 