package org.vanme;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonTest {

    @Nested
    class FullNameTestCase {

        @Test
        void shouldReturnLastNameFollowedByFirstName() {
            Person person = new Person("Vito", "CORLEONE");

            assertThat(person.getFullName())
                    .isEqualTo("CORLEONE Vito");
        }

        @Test
        void shouldReturnLastNameInUpperCase() {
            Person person = new Person("Vito", "Corleone");

            assertThat(person.getFullName())
                    .isEqualTo("CORLEONE Vito");
        }
    }
}
