package org.vanme;

import static java.util.Objects.requireNonNull;

public record Person(String firstName, String lastName) {

    public Person {
        requireNonNull(firstName);
        requireNonNull(lastName);
    }

    public String getFullName() {
        return "%s %s".formatted(lastName.toUpperCase(), firstName);
    }
}
